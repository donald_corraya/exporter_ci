<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Company_export extends CI_Controller{
        
        function __construct() {
            parent::__construct();
            $this->load->model("General_model");
        }
        
        public function index() {
            
            $this->form_validation->set_rules('country', 'Country', 'required');
            
            if ($this->form_validation->run()){
                $country = $this->input->post('country');
                
                $email_arr = array();
                
                $whr = array(
                    'registrant_country' => $country
                );
                
                $result = $this->General_model->select("*",$whr ,'all_user_data');
                $data['details_data'] = self::csv_create($result,$country);
            }
            
            $result = $this->General_model->select('*','' ,'countries','name','asc');
            if($result){
                $data['countries'] = $result;
            }else{
                echo "Country does not exist";
                exit();
            }
            
            
            
            $data['company_export'] = 'company_export';
            $this->load->view('template',$data);
        }
        
        public function csv_create($result,$country) {
            $all_data = array();
            $i = 0;
            $output[] = array("Id", "Name", "Company", "Address", "State", "Zip", "Country", "Phone", "Email", "City");
            foreach ($result as $details){
                $i++;
                $all_data[] = $i.". ".$details->resgistrant_name." = ".$details->registrant_email."<br>";
                
                $output[] = array(
                    $details->id, 
                    str_replace(";",",",$details->resgistrant_name),
                    str_replace(";",",",$details->registrant_company),
                    str_replace(";",",",$details->registrant_address),
                    str_replace(";",",",$details->registrant_state),
                    $details->registrant_zip,                    
                    $details->registrant_country,
                    $details->registrant_phone,
                    $details->registrant_email,
                    $details->registrant_city,
                );
            }
            $dir = "csv_file";
            if (!is_dir($dir)) {
                mkdir($dir);
            }
            $dir = "csv_file/".$country."/";                
            if (!is_dir($dir)) {
                mkdir($dir);
            }
            $dir.=$country;
            //echo $dir;die;
            $file = fopen($dir.".csv","w");
            foreach ($output as $line) {
                fputcsv($file, $line);
            }
            fclose($file);
            return $all_data;
        }
        
        public function ajax(){
            if (!$this->input->is_ajax_request()) {
                exit('No direct script access allowed');
            }
            $country = $this->input->post('country');
            
            /* Checking is array or not */
            if(is_array($country)){
                $i = 1;
                $e = 1;
                $c = 1;
                $email = "";
                $coun = "";
                foreach ($country as $arr_details){
                    if($i == 1){
                        foreach ($arr_details as $emails){
                            if($e>1){
                                $email = $email." OR registrant_email LIKE '%$emails'";
                            }else{
                                $email = $email."(registrant_email LIKE '%$emails'";
                            }
                            
                        $e++;
                        }
                    }else{
                       foreach ($arr_details as $countries){
                           if($c>1){
                                $coun = $coun." OR registrant_country='$countries'";
                            }else{
                                $coun = $coun."(registrant_country='$countries'";
                            }
                       $c++;
                       }
                    }
                    
                    $i++;
                }
                $coun = $coun.")";
                $email = $email.")";
                $whr = "$coun AND $email";
            }else{
               $whr = array(
                    "registrant_country" => $country
                );  
            }  
            $result = $this->General_model->select('*', $whr ,'all_user_data');
            //echo $this->db->last_query();die;
            $data_count = count($result);
            echo "<b>Total record = ".$data_count."</b>";
        }
        
        public function keyword_search(){
           $this->form_validation->set_rules('name', 'Name', 'required');
           $this->form_validation->set_rules('column', 'Column', 'required');            
            if ($this->form_validation->run()){
                $name = $this->input->post('name');
                $column = $this->input->post('column');
                if($column == "name_email"){
                    $whr = "registrant_company LIKE '%".$name."%' or registrant_email LIKE '%".$name."%'";
                }elseif($column == "registrant_email"){
                    $whr = "registrant_email LIKE '%".$name."%'";
                }else{                    
                    $whr = "registrant_company LIKE '%".$name."%'";
                }                
                $result = $this->General_model->select('*', $whr ,'all_user_data'); 
                $data['details_data'] = self::csv_create($result,$column);
            }
            
           $data['keyword_search'] = 'keyword_search';
           $this->load->view('template',$data); 
        }
        public function email_country() {           
           $this->form_validation->set_rules('email_one', 'Email 1', 'required');
           $this->form_validation->set_rules('email_two', 'Email 2', '');
           $this->form_validation->set_rules('email_three', 'Email 3', '');
           $this->form_validation->set_rules('email_four', 'Email 4', '');
           $this->form_validation->set_rules('email_five', 'Email 5', '');
           
           $this->form_validation->set_rules('country_one', 'Country 1', 'required');
           $this->form_validation->set_rules('country_two', 'Country 2', '');
           $this->form_validation->set_rules('country_three', 'Country 3', '');
           $this->form_validation->set_rules('country_four', 'Country 4', '');
           $this->form_validation->set_rules('country_five', 'Country 5', '');
           
           if ($this->form_validation->run()){
               $email_one = $this->input->post('email_one');
               $email_two = $this->input->post('email_two');
               $email_three = $this->input->post('email_three');
               $email_four = $this->input->post('email_four');
               $email_five = $this->input->post('email_five');
               
               $country_one = $this->input->post('country_one');
               $country_two = $this->input->post('country_two');
               $country_three = $this->input->post('country_three');
               $country_four = $this->input->post('country_four');
               $country_five = $this->input->post('country_five');
               $whr = "(";
               if(isset($email_one) and !empty($email_one)){  
                    $whr = $whr."registrant_email LIKE '%$email_one%'";
               }else{
                   die("Email 1 is not set or empty");
               }
               
               if(isset($email_two) and !empty($email_two)){                   
                   $whr = $whr." OR registrant_email LIKE '%$email_two%'";                             
               }
               
               if(isset($email_three) and !empty($email_three)){                   
                   $whr = $whr." OR registrant_email LIKE '%$email_three%'";                             
               }
               
               if(isset($email_four) and !empty($email_four)){                   
                   $whr = $whr." OR registrant_email LIKE '%$email_four%'";                             
               }
               
               if(isset($email_five) and !empty($email_five)){                   
                   $whr = $whr." OR registrant_email LIKE '%$email_five%'";                             
               }
               $whr= $whr.") AND (";
               
               if(isset($country_one) and !empty($country_one)){                   
                   $whr = $whr." registrant_country='$country_one'";                                
               }else{
                   die("Country 1 is not set or empty");
               }
               
               if(isset($country_two) and !empty($country_two)){                   
                   $whr = $whr." OR registrant_country='$country_two'";                                
               }
               
               if(isset($country_three) and !empty($country_three)){                   
                   $whr = $whr." OR registrant_country='$country_three'";                                
               }
               
               if(isset($country_four) and !empty($country_four)){                   
                   $whr = $whr." OR registrant_country='$country_four'";                                
               }
               
               if(isset($country_five) and !empty($country_five)){                   
                   $whr = $whr." OR registrant_country='$country_five'";                                
               }
               
               $whr= $whr.")";               
               $result = $this->General_model->select('*', $whr ,'all_user_data');
               $data['details_data'] = self::csv_create($result,"email_country");               
           }
           
           $result = $this->General_model->select('*','' ,'countries','name','asc');
        if($result){
            $data['countries'] = $result;
        }else{
            echo "Country does not exist";
            exit();
        }
           
           $data['email_country'] = 'email_country';
           $this->load->view('template',$data); 
        }
        
    }
?>

