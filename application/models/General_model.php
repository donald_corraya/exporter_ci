<?php

    class General_model extends CI_Model{
        
        
        public function select($select,$whr='' ,$table,$order_by='',$ac_dc=''){
        $this->db->select($select);
        $this->db->from($table);
        if(!empty($whr)) {
            $this->db->where($whr);
        }
        
        if($order_by){
            $this->db->order_by($order_by,$ac_dc);
        }
        
        $result = $this->db->get();
        if ($result) {
            return $result->result();
        } else {
            return FALSE;
        }
    }
        
        
    }

?>
