<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="<?= base_url()?>assets/includes/js/custom.js"></script>
<link rel="stylesheet"  href="<?= base_url()?>assets/includes/css/bootstrap.css">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?= base_url()?>">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class=""><a href="<?= base_url()?>">Company Export</a></li>
      <li class=""><a href="<?= base_url()?>company_export/keyword_search">Keyword Search</a></li>
      <li class=""><a href="<?= base_url()?>company_export/email_country">Email + Country</a></li>
    </ul>
  </div>
</nav>

<?php
    if(isset($company_export)){        
        $this->load->view('pages/company_export');
    }elseif(isset($keyword_search)) {
        $this->load->view('pages/keyword_search');        
    }elseif($email_country) {
        $this->load->view('pages/email_country');
    }
?>
