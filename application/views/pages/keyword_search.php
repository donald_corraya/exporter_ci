<style type="text/css">
    label{
        display: block!important;
    }
</style>
<div class="container-fluid">
    <div class="col-lg-12 list-inline">        
        <h3>Export the Companies Files</h3>
        <form method="post" id="country_update" action="" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-2">
                    <label>Name:
                        <input type="text" name="name" class="form-control">
                        <span class="text-danger"><?= form_error('name');?></span>
                    </label>
                </div>
                <div class="col-lg-2">
                    <label>Please Select Column:
                        <select name="column" class="form-control country" style="width: 100%;">
                            <option value="resgistrant_name"> Company Name</option>
                            <option value="registrant_email"> By Email</option>
                            <option value="name_email"> By Both</option>	                    
                        </select>                        
                    </label>
                </div>
                <div class="col-lg-2" style="margin-top: 19px;">
                    <button class="btn btn-primary" type="submit">Export</button>                    
                </div>                
                <div id="results" class="col-lg-6">                   
                    <?php
                        if(!empty($details_data)){
                            foreach ($details_data as $result){
                                echo $result;
                            }
                        }
                    ?>                    
                </div>
            </div>
        </form>    
    </div>
</div>