<div class="container-fluid">
    <div class="col-lg-10 list-inline">        
        <h3>Email + Country</h3>
        <form method="post" id="country_update" action="" enctype="multipart/form-data">            
            <div class="col-lg-12">
                <div class="col-lg-2">
                    <label>Email 1:
                        <input type="text" name="email_one" class="form-control">
                        <span class="text-danger"><?= form_error('email_one');?></span>
                    </label>
                </div>
                <div class="col-lg-2">
                    <label>Email 2:
                        <input type="text" name="email_two" class="form-control">
                        <span class="text-danger"><?= form_error('email_two');?></span>
                    </label>
                </div>
                <div class="col-lg-2">
                    <label>Email 3:
                        <input type="text" name="email_three" class="form-control">
                        <span class="text-danger"><?= form_error('email_three');?></span>
                    </label>
                </div>
                <div class="col-lg-2">
                    <label>Email 4:
                        <input type="text" name="email_four" class="form-control">
                        <span class="text-danger"><?= form_error('email_four');?></span>
                    </label>
                </div>
                <div class="col-lg-2">
                    <label>Email 5:
                        <input type="text" name="email_five" class="form-control">
                        <span class="text-danger"><?= form_error('email_five');?></span>
                    </label>
                </div>
            </div>
                
            <div class="col-lg-12">
                <div class="col-lg-2">                    
                    <label>Country 1:
                        <select name="country_one" class="form-control country" style="width: 100%;">
                        <option value="">Select</option>
                        <?php foreach ($countries as $country){ ?>
                        <option value="<?= $country->name?>"><?= $country->name?></option>
                        <?php } ?>
                        </select>
                        <span class="text-danger"><?= form_error('country');?></span>
                    </label>
                </div>
                
                <div class="col-lg-2">
                    <label>Country 2:
                        <select name="country_two" class="form-control country" style="width: 100%;">
                        <option value="">Select</option>
                        <?php foreach ($countries as $country){ ?>
                        <option value="<?= $country->name?>"><?= $country->name?></option>
                        <?php } ?>
                        </select>
                        <span class="text-danger"><?= form_error('country');?></span>
                    </label>
                    
                </div>
                
                <div class="col-lg-2">
                    <label>Country 3:
                        <select name="country_three" class="form-control country" style="width: 100%;">
                        <option value="">Select</option>
                        <?php foreach ($countries as $country){ ?>
                        <option value="<?= $country->name?>"><?= $country->name?></option>
                        <?php } ?>
                        </select>
                        <span class="text-danger"><?= form_error('country');?></span>
                    </label>                    
                </div>
                
                <div class="col-lg-2">
                    <label>Country 4:
                        <select name="country_four" class="form-control country" style="width: 100%;">
                        <option value="">Select</option>
                        <?php foreach ($countries as $country){ ?>
                        <option value="<?= $country->name?>"><?= $country->name?></option>
                        <?php } ?>
                        </select>
                        <span class="text-danger"><?= form_error('country');?></span>
                    </label>                     
                </div>
                
                <div class="col-lg-2">                    
                    <label>Country 5:
                        <select name="country_five" class="form-control country" style="width: 100%;">
                        <option value="">Select</option>
                        <?php foreach ($countries as $country){ ?>
                        <option value="<?= $country->name?>"><?= $country->name?></option>
                        <?php } ?>
                        </select>
                        <span class="text-danger"><?= form_error('country');?></span>
                    </label>   
                </div>
                <div class="col-lg-12">
                    <button class="btn btn-primary" type="submit">Export</button> 
                    <button id="check_data_ec" class="btn btn-success" type="button">Check Data</button>
                </div>
            </div>
        </form>
                <div id="results" class="col-lg-12">   
                    <div class="col-lg-12">                        
                        <?php
                            if(!empty($details_data)){
                                foreach ($details_data as $result){
                                    echo $result;
                                }
                            }
                        ?>     
                    </div>
                </div>
                
                </div>            
            
    </div>
</div>