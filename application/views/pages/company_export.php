<div class="container-fluid">
    <div class="col-lg-12 list-inline">        
        <h3>Export Data</h3>
        <form method="post" id="country_update" action="" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-2">
                    <label>Country:
                        <select name="country" class="form-control country" style="width: 100%;">
                        <option value="0">Select</option>
                        <?php foreach ($countries as $country){ ?>
                        <option value="<?= $country->name?>"><?= $country->name?></option>
                        <?php } ?>
                        </select>
                        <span class="text-danger"><?= form_error('country');?></span>
                    </label>
                </div>
                <div class="col-lg-2" style="margin-top: 19px;">
                    <button class="btn btn-primary" type="submit">Submit</button>
                    <button id="check_data" class="btn btn-success" type="button">Check Data</button>
                </div>
                    <div id="results" class="col-lg-6">                   
                    <?php
                        if(!empty($details_data)){
                            foreach ($details_data as $result){
                                echo $result;
                            }
                        }
                    ?>                    
                </div>
            </div>
        </form>    
    </div>
</div>