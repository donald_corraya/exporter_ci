$(document).on("click", "#check_data", function(){
    $("#results").html("Loading...");
    var country = $('.country').find(":selected").val();
    var url = window.location.href; 
    url = url+"company_export/ajax";
    console.log(url+"/ajax");
    ajax(url,country);
});
var email_one="",
    email_two="",
    email_three='',
    email_four='',
    email_five='',
    country_one='',
    country_two='',
    country_three='',
    country_four='',
    country_five='';

$(document).on("click", "#check_data_ec", function(){
    $("#results").html("Loading...");
    var country = $('.country').find(":selected").val();
    var url = "http://localhost/exporter_ci/company_export/ajax";  
    
    if($('input[name="email_one"]').val()){
        email_one = $('input[name="email_one"]').val();
    }
    
    if($('input[name="email_two"]').val()){
        email_two = $('input[name="email_two"]').val();
    }
    
    if($('input[name="email_three"]').val()){
        email_three = $('input[name="email_three"]').val();
    }
    
    if($('input[name="email_four"]').val()){
        email_four = $('input[name="email_four"]').val();
    }
    
    if($('input[name="email_five"]').val()){
        email_five = $('input[name="email_five"]').val();
    }
    
    /*Country */
    if($('select[name="country_one"]').val()){
        country_one = $('select[name="country_one"]').val();
    }
    
    if($('select[name="country_two"]').val()){
        country_two = $('select[name="country_two"]').val();
    }
    
    if($('select[name="country_three"]').val()){
        country_three = $('select[name="country_three"]').val();
    }
    
    if($('select[name="country_four"]').val()){
        country_four = $('select[name="country_four"]').val();
    }
    
    if($('select[name="country_five"]').val()){
        country_five = $('select[name="country_five"]').val();
    }
    
    var emails = [email_one,email_two,email_three,email_four,email_five];
    var countries = [country_one,country_two,country_three,country_four,country_five];    
    
    //filtering data
    emails = emails.filter(item => item);
    countries = countries.filter(item => item);
    
    var all_data = [emails,countries];
    ajax(url,all_data);
    //console.log(all_data);    
});


function ajax(url,country){
    //console.log(url,country);
    
    $.ajax({
        url: url,
        type: "post",
        cache: false,
        data:{
          country: country
        },
        success: function(html){
            //console.log(html);
          $("#results").html(html);
        }
    });
}