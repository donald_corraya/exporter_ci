<?php
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $url = explode("http://localhost/export/", $actual_link);

    if($url[1] == ''){
        $url[1] = 'index.php';
    }
?>
<link rel="stylesheet"  href="includes/css/bootstrap.css">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="<?=($url[1] == 'index.php')? 'active' : ''?>"><a href="index.php">Company Export</a></li>
      <li class="<?=($url[1] == 'consumer.php')? 'active' : ''?>"><a href="consumer.php">Consumer Export</a></li>
      <li class="<?=($url[1] == 'email-restrict.php')? 'active' : ''?>"><a href="email-restrict.php">Email Restricted</a></li>
      <li class="<?=($url[1] == 'keyword.php')? 'active' : ''?>"><a href="keyword.php">Keyword Search</a></li>
      <li class="<?=($url[1] == 'email_country.php')? 'active' : ''?>"><a href="email_country.php">Email + Country</a></li>
      <li class="<?=($url[1] == 'check_duplicate.php')? 'active' : ''?>"><a href="check_duplicate.php">Duplicate Email Check</a></li>
    </ul>
  </div>
</nav>
